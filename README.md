# __midge__

A graphical development engine and editor environment intended for use with machine _"intelligent"_ systems.

#### Focus on visualization of data and errors
Allows quick reiteration and fixing of problems.

#### Automation of process and workflow generation

Using historical tracking of utilized actions and processes for statistical
suggestion for collaboration with the user to construct workflow processes. These
processes can be used for guidance and prediction of future user behaviour through
repeatable tasks and workflows. This reduces mechanical and cognitive expense to the
developer to increase productivity.

#### Interface for AI collaboration

Providing the interactive means to develop in collaboration with increasingly useful
data science ('2.0') functions to produce software faster.

#### Dynamic recompilation
  
Using the [cling c++ interpreter](https://root.cern.ch/cling) to enable dynamic
evaluation and quicker readjustments of projects.

#### Maximal configuration of development environment
  
The development environment itself (as much as possible) is run inside the same 
process used to develop projects. Utilizing the dynamic recompilation allows
reconfiguration by the developer however it may serve best. This allows editing
of the development environment by using the development environment.

## Progress

_This project is still very much in the discovery process and no useful releases are
in any immediate future._

## Installation

_no concrete instructions yet_

* __Requires:__
- [cling c++ interpreter](https://root.cern.ch/cling)
- [cglm](https://github.com/recp/cglm)
- [vulkan sdk](https://www.lunarg.com/vulkan-sdk/)