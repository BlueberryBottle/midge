
#include "env/environment_definitions.h"
#include "render/render_thread.h"
#include "ui/ui_definitions.h"

typedef struct modus_operandi_data {
  mc_node *node;

  struct {
    unsigned int width, height;
    mcr_texture_image *image;
  } render_target;

} modus_operandi_data;

void _mco_render_mo_data_headless(mc_node *node)
{
  modus_operandi_data *modata = (modus_operandi_data *)node->data;

  // Children
  for (int a = 0; a < node->children->count; ++a) {
    mc_node *child = node->children->items[a];
    if (child->layout && child->layout->visible && child->layout->render_headless &&
        child->layout->__requires_rerender) {
      // TODO fptr casting
      void (*render_node_headless)(mc_node *) = (void (*)(mc_node *))child->layout->render_headless;
      render_node_headless(child);
    }
  }

  // Render the render target
  global_root_data *global_data;
  obtain_midge_global_root(&global_data);

  image_render_details *irq;
  mcr_obtain_image_render_request(global_data->render_thread, &irq);
  irq->render_target = NODE_RENDER_TARGET_IMAGE;
  irq->clear_color = COLOR_DEEP_FIR;
  // printf("global_data->screen : %u, %u\n", global_data->screen.width,
  // global_data->screen.height);
  irq->image_width = modata->render_target.width;   // TODO
  irq->image_height = modata->render_target.height; // TODO
  irq->data.target_image.image = modata->render_target.image;
  irq->data.target_image.screen_offset_coordinates.x = (unsigned int)node->layout->__bounds.x;
  irq->data.target_image.screen_offset_coordinates.y = (unsigned int)node->layout->__bounds.y;

  // Children
  for (int a = 0; a < node->children->count; ++a) {
    mc_node *child = node->children->items[a];
    if (child->layout && child->layout->visible && child->layout->render_present) {
      // TODO fptr casting
      void (*render_node_present)(image_render_details *, mc_node *) =
          (void (*)(image_render_details *, mc_node *))child->layout->render_present;
      render_node_present(irq, child);
    }
  }

  mcr_submit_image_render_request(global_data->render_thread, irq);
}

void _mco_render_mo_data_present(image_render_details *image_render_queue, mc_node *node)
{
  modus_operandi_data *modata = (modus_operandi_data *)node->data;

  mcr_issue_render_command_textured_quad(image_render_queue, (unsigned int)node->layout->__bounds.x,
                                         (unsigned int)node->layout->__bounds.y, modata->render_target.width,
                                         modata->render_target.height, modata->render_target.image);
}

void _mco_handle_input(mc_node *node, mci_input_event *input_event)
{
  // printf("_mco_handle_input\n");
  input_event->handled = true;
  if (input_event->type == INPUT_EVENT_MOUSE_PRESS || input_event->type == INPUT_EVENT_MOUSE_RELEASE) {
    input_event->handled = true;
    mca_focus_node(node);
  }
}

void mco_load_resources(mc_node *module_node)
{
  // cube_template
  modus_operandi_data *mo_data = (modus_operandi_data *)malloc(sizeof(modus_operandi_data));
  module_node->data = mo_data;
  mo_data->node = module_node;

  mo_data->render_target.image = NULL;
  mo_data->render_target.width = module_node->layout->preferred_width;
  mo_data->render_target.height = module_node->layout->preferred_height;
  mcr_create_texture_resource(mo_data->render_target.width, mo_data->render_target.height,
                              MVK_IMAGE_USAGE_RENDER_TARGET_2D, &mo_data->render_target.image);
}

void init_modus_operandi(mc_node *app_root)
{
  global_root_data *global_data;
  obtain_midge_global_root(&global_data);

  //   instantiate_all_definitions_from_file(app_root, "src/modules/source_editor/source_line.c", NULL);

  mc_node *node;
  mca_init_mc_node(app_root, NODE_TYPE_ABSTRACT, &node);
  mca_init_node_layout(&node->layout);
  node->children = (mc_node_list *)malloc(sizeof(mc_node_list));
  node->children->count = 0;
  node->children->alloc = 0;
  node->layout->preferred_width = 400;
  node->layout->preferred_height = 500;

  node->layout->padding.left = 120;
  node->layout->padding.bottom = 80;
  node->layout->horizontal_alignment = HORIZONTAL_ALIGNMENT_LEFT;
  node->layout->vertical_alignment = VERTICAL_ALIGNMENT_BOTTOM;

  node->layout->determine_layout_extents = (void *)&mca_determine_typical_node_extents;
  node->layout->update_layout = (void *)&mca_update_typical_node_layout;
  node->layout->render_headless = (void *)&_mco_render_mo_data_headless;
  node->layout->render_present = (void *)&_mco_render_mo_data_present;
  node->layout->handle_input_event = (void *)&_mco_handle_input;

  mca_set_node_requires_layout_update(node);

  mco_load_resources(node);
  // Panel
  // mcu_panel *panel;
  // mcu_init_panel(global_data->global_node, &panel);

  // panel->node->layout->padding = {300, 400, 800, 60};
  // panel->background_color = COLOR_GREEN;

  // mcu_set_element_update(panel->element);

  // Text Block
  // mcu_text_block *text_block;
  // mcu_init_text_block(global_data->global_node, &text_block);

  // mca_node_layout *layout = text_block->element->layout;
  // layout->horizontal_alignment = HORIZONTAL_ALIGNMENT_LEFT;
  // layout->vertical_alignment = VERTICAL_ALIGNMENT_TOP;
  // layout->padding = {150, 300, 0, 0};

  // set_c_str(text_block->str, "");
  // for (int a = 32; a < 128; ++a) {
  //   char buf[2];
  //   buf[0] = (char)a;
  //   buf[1] = '\0';
  //   append_to_c_str(text_block->str, buf);
  // }
  // text_block->font_color = COLOR_LIGHT_YELLOW;
}